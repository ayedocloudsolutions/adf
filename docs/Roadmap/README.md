# Roadmap

## Next

- [ ] Illustrate concept of review apps
- [ ] Merge Request Workflow
  - [ ] Branching Workflow
- [ ] Repository structure lint
  - [ ] Files exist
  - [ ] Folders exist
  - [ ] Files not empty

## Now

- [ ] Repository structure
  - [ ] .ci/
  - [ ] .vscode
    - [ ] extensions.json
    - [ ] settings.json
  - [ ] .gitlab/
    - [ ] issue_templates/
  - [ ] README.md
  - [ ] Dockerfile
  - [ ] docker-entrypoint.sh
  - [ ] .gitignore
  - [ ] .dockerignore
  - [ ] .gitlab-ci.yml
  - [ ] .releaserc.yml
  - [ ] CHANGELOG.md
  - [ ] CODEOWNERS
  - [ ] CONTRIBUTING.md
  - [ ] LICENSE
  - [ ] mkdocs.yml
  - [ ] tests/
  - [ ] src/
  - [ ] docs/
    - [ ] README.md
    - [ ] Roadmap/
      - [ ] README.md

## Done ✓

- [x] Completed task title
