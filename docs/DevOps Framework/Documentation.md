# README basics

```1. Add your documentation
2. 8. How to build the code
3. How to build the image
4.  How to run the code
5.  How to run the image
6.  How to install the Helm chart```

```markdown
# $PROJECT_NAME

[[_TOC_]]

$PROJECT_NAME is thing doing things.

## Getting started

### Prerequisites

- any external dependencies like databases, caches, etc
- any local tool needed to build and run the service
- any package that has to be available

### Installation

[how to install the service/software - list dependencies and prerequisites here]

\```bash
# Local
git clone $PROJECT_URL
cd $PROJECT
dotnet build

# Kubernetes
helm install $PROJECT_NAME $PROJECT_URL/charts
\```

## Usage

[a complete list of settings and configuration options and their defaults the service consumes]

| Parameter      | Example   | Description           | Default          |
| -------------- | --------- | --------------------- | ---------------- |
| `DatabaseHost` | `db:3306` | The database hostname | `localhost:3306` |

## Architecture

[high-level information about the service/software]

## Contributing

[specified either in CONTRIBUTING.md or linked to an external source]

## Codeowners

[optional: clear information who's responsible for this service]

## License

MIT
```
