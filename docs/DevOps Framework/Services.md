# Services

A service in context of the DevOps Framework is an application serving clients, for example a http-based microservice offering an API.

## Guidelines

In microservice architectures, services often exist multiple times in the same cluster or even deployment. Debugging runtime issues in distributed environments can quickly become very painful if the services being debugged do not integrate well with their ecosystem. Thus, service owners and developers should follow a few guidelines to ease interaction with and operations of their services.

We assume that a service will be http-based and run in a container inside a Kubernetes cluster. This implies a few infrastructral prerequisites (Kubernetes services, a log aggregation service, a metrics service) have to be there for the guidelines to work to their full potential.

- a service **must** be built with their build version baked into the artifact
- a service **must** accept all configuration through environment variables
- a service **must** log to STDOUT only
- a service **must** log its name and build version on startup before doing anything else
- a service **must** support multiple loglevels (INFO, WARNING, CRITICAL, DEBUG)
- a service **must** use the default ports 80 and 443
- a service **must** expose a health endpoint on /health or /healthz
- a service **must** wait for external dependencies to be available for at least 60 seconds before exiting
- a service **must** exit gracefully
- a service **must** be scalable to multiple load-balanced replicas (unless it's stateful)
- a service **must** support zero downtime upgrades (migrate database and deploy new version of service before old version is removed)
- a service **should** provide integration tests
- a service **should** emit traces
- a service **should** expose a prometheus compatible metrics endpoint on /metrics
- a service **should not** implement TLS-handling but instead rely on a service mesh or a reverse proxy
- database migrations **must not** be part of the service logic
- database migrations **must** be executed as a dedicated job before a service starts
- exceptions **must not** cause stack traces
- exceptions **must** be caught and a succinct, human readable message must be printed to STDOUT
- health endpoints **must** expose a service's build version and date
- health endpoints **must** check dependencies (like databases, other services, etc)

## Packaging

- kubernetes.app annotation

## Tips

- HEALTHCHECK in Dockerfile
- init-container + Kubernetes Job > HELM pre-upgrade hook
- .NET EntityFramework  for databases
- .NET Prometheus and Healthcheck integration
- build .NET with version in binary
