# Healthchecks

An application should run internal healthchecks to check for dependency availability and anything related to its ability to serve requests properly.

By default, healthchecks should be exposed as http endpoints to give external services (like Kubernetes) a chance to assess the service's status. The endpoint should be `/health`, `/healthz` or `/ready`and available on port `80`.

## Languages

Most programming languages come with a library to instrument healthchecks.

### .NET/C#

- Library: [HealthChecks](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-5.0)

#### Example

```c#
public class BasicStartup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddHealthChecks();
    }

    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapHealthChecks("/health");
        });
    }
}
```