# Monitoring

Exposing metrics from your application can be quite helpful to understand its performance und issues at runtime. The inudstry standard for gathering timeseries-based metrics is [Prometheus](https://prometheus.io). This is what will be available to the application components as part of the platform.

Metrics are typically custom values that depend on the application's structure and usecase. There's often a default set of metrics that can be exposed (like memory usage, etc), but the power of exposing metrics lies in custom metrics that help you understand and monitor your application in a business context.

By default, Prometheus expects metrics of a service to be found on a http endpoint `/metrics` on port `80` so it makes sense to have your application expose metrics on that path and port.

## Languages

Most programming languages come with a library to instrument metrics exposure.

### .NET/C#

- Library: [Prometheus .NET](https://github.com/prometheus-net/prometheus-net)

#### Example

```c#
using Prometheus;
using System;
using System.Threading;

class Program
{
    private static readonly Counter TickTock =
        Metrics.CreateCounter("sampleapp_ticks_total", "Just keeps on ticking");

    static void Main()
    {
        var server = new MetricServer(hostname: "localhost", port: 1234);
        server.Start();

        while (true)
        {
            TickTock.Inc();
            Thread.Sleep(TimeSpan.FromSeconds(1));
        }
    }
}
```