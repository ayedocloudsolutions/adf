# Tracing

In a distributed scenario where probably multiple replicas of a single service exists, distributed tracing helps making sense of what happens insight the broader application context.

Distributed tracing is possible thanks to tools like [Jaeger](https://www.jaegertracing.io) and [Grafana Tempo](https://grafana.com/oss/tempo/). 

Both systems support the [Open Telemetry Standard](https://opentelemetry.io) that can be used to emit traces to these services with a library.

A service instrumenting distributed tracing needs to be aware of the trace store und thus be configured by environment to be able to reach the service. 

Tracing should be optionally configurable by environment variable.

## Languages

Most programming languages come with a library to instrument trace emission.

### .NET/C#

- Library: [OpenTelemetry .NET](https://github.com/open-telemetry/opentelemetry-dotnet)

Additionally, it's possible that you need a Telemetry Exporter for the distributed tracing system:

- [Jaeger Exporter](https://github.com/open-telemetry/opentelemetry-dotnet/blob/main/src/OpenTelemetry.Exporter.Jaeger/README.md)

> Note: the Jaeger Exporter works for Grafana Tempo, too

#### Config

- `OTEL_EXPORTER_JAEGER_AGENT_HOST`
- `OTEL_EXPORTER_JAEGER_AGENT_PORT`

#### Example

```c#
using System.Diagnostics;
using OpenTelemetry;
using OpenTelemetry.Trace;

public class Program
{
    private static readonly ActivitySource MyActivitySource = new ActivitySource(
        "MyCompany.MyProduct.MyLibrary");

    public static void Main()
    {
        using var tracerProvider = Sdk.CreateTracerProviderBuilder()
            .SetSampler(new AlwaysOnSampler())
            .AddSource("MyCompany.MyProduct.MyLibrary")
            .AddConsoleExporter()
            .Build();

        using (var activity = MyActivitySource.StartActivity("SayHello"))
        {
            activity?.SetTag("foo", 1);
            activity?.SetTag("bar", "Hello, World!");
            activity?.SetTag("baz", new int[] { 1, 2, 3 });
        }
    }
}
```