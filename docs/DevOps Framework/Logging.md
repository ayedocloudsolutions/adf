# Logging & Error handling

Error handling, alongside logging, must be one of the first things to get right. When starting with development, the relevance of verbose output for people other than the programmer debugging the application (e.g. in production) is not that high because most of the time, you'll be the only one working with the code.

Still, you should plan for a growing team and thus be as verbose and strict with errors as possible right from the beginning.

## Logging

- log everything
- establish different [loglevels](https://www.section.io/engineering-education/how-to-choose-levels-of-logging/)
  - default is `information` (or `INFO`)
- expose the loglevels through [configuration](Configuration.md)
  - enable the user/operator to increase verbosity with configuration

## Error handling

- try/catch everything you do in the code
- properly transport exceptions (the thing that actually broke your code) to the user in the form of human readable logs
  - no stack trace - EVER
- combine this with tracing wherever possible
- human readble means the human operating the software is able to derive fixes from the way your application signals a problem
