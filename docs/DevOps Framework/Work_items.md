# Work items

## General rules

1. The maximum number of work in progress depends on the most constrained part of the process (the bottleneck)
2. Only if a work item passes the bottle neck, a new one may be pulled into the system (you pull work in progress by committing on a work item)
3. If you don't know the bottleneck yet, try to not have more than 5 work items in progress

## GitLab

In GitLab, use the following process to keep WIP in flow:

1. Create an issue for a high-level task (user story)
2. Track additional tasks as they arise as a task list inside the issue description
3. Add label `Commited` to the issue once you commit to work on it
4. Create a merge request containing all changes to code and documentation relevant to this work item
5. Add label `Ready` to the issue once it's ready for delivery
6. Add label `Released` to the issue once it's delivered (remove label `Ready`)
7. Remove any of these labels if you're not comitting on the issue anymore
8. Close the issue