# Source code

The source code of the application - i.e. the business logic. Source code must be located in a directory called `src/`.

The source code defines what the application is doing and how. Additionally, it integrates the application with its operating environments.

Cloud native applications are deeply integrated into their runtime environment, typically accompanied by tools like prometheus, loki, etc. Depending on the language the application is built in, different changes have to be done to the source code to integrate with these tools.

## Oberservability

Debugging applications at runtime is hard, but there's an integrated toolchain available to ease the analysis and forecasting of issues at runtime:

- [monitoring](Monitoring.md)
- [logging](Logging.md)
- [tracing](Tracing.md)