# DevOps Framework

## About

This is a framework for developing cloud native applications from **idea to production**.

## Table of contents

[[ __TOC__ ]]

## What is an application

See [Application](Application.md)

## Get started

An applications goes through a [lifecycle](Lifecycle.md), starting with the **idea** and hopefully ending in a **production** deployment. The application evolves through the changes that pass this lifecycle.

- Understand the [application lifecycle](Lifecycle.md)
- Get used to the [development workflow](Development.md)
