# Repository

> make sure to follow the [naming conventions](Naming.md) when creating new repositories

## Structure

```bash
├── .vscode/
│   ├── extensions.json
│   └── settings.json
├── .gitlab/
│   └── issue_templates/
├── docs/
│   ├── README.md
│   └── Roadmap/
│       └── README.md
├── src/
├── tests/
├── .dockerignore
├── .gitignore
├── .gitlab-ci.yml
├── .releaserc.yml
├── Dockerfile
├── docker-entrypoint.sh
├── CHANGELOG.md
├── CODEOWNERS
├── CONTRIBUTING.md
├── LICENSE
├── mkdocs.yml
└── README.md
```

### .vscode

This folder contains settings and extension recommendations for [Visual Studio Code](https://code.visualstudio.com). These help to establish a few standards amongst all developers working with the repository.

### .gitlab

This folder contains settings for [GitLab](https://gitlab.com), for example issue and merge-request templates or CI configuration.

### docs

This folder contains documentation in [Markdown](https://www.markdownguide.org). The directory content will be built with a static site generator like [MkDocs](https://www.mkdocs.org) (which is the default) to serve as source for continuous documentation.

Keeping [documentation](Documentation.md) close to the [source code](Sourcecode.md) keeps it in sync with the development cycles and reduces context switches (and thus: avoidance) for developers. Exporting it with a static site generator enables direct delivery to stakeholders and customers and enables continuous quality assurance with the help of [review apps](Roadmap.md).

### src

This folder contains the [source code](Sourcecode.md) of the application.

### tests

This folder contains the [tests](Lifecycle.md) of the application.

### .dockerignore

This file works like `.gitignore` - it enables you to specify files or folders to be ignored by docker on build.

### .gitignore

This file enables you to specify files or folders to be ignored by git.

### .gitlab-ci.yml

[GitLab](https://gitlab.com) CI configuration file. This should ideally include a dedicated (see `.gitlab/`) or remote hosted CI file.

### .releaserc.yml

This is the configuration file for [semantic-release](Versioning.md), used for automated versioning.

## Remote repositories

With git, it's possible to have multiple remote repositories associated with a local repository. The default remote is called `origin`. The [default branch](Branches.md) ist `main`.

### Add remote repository

```bash
git remote add origin https://registry.gitlab.com/ayedocloudsolutions/adf.git
```

### Pull from remote repository

```bash
git pull origin main
```

### Push to remote repository

```bash
git push origin main
```
