# Packaging

Packaging is the process of preparing an application for a certain runtime environment. Examples would be:

- Helm charts for Kubernetes
- Container images for Docker
- apt for debian/ubuntu
- brew for macOS

## Container

An application must be packaged as a container.

## Helm charts

An application must be packaged as a Helm chart.
