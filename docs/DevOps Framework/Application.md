# What is an application

An application is a usable asset that takes input and produces output. An application can be composed of other applications. In the context of the [DevOps Framework](README.md) an application is a service that will be used by a client over the network.

An application has a loose structure and a set of required components. A minimum viable application must constist of:

- [Documentation](Documentation.md)
- [Source code](Sourcecode.md)
- [Packaging](Packaging.md)
- [Ownership](Ownership.md)
- [Contribution guidelines](Contribution_guidelines.md)

## Inputs

An application can have multiple inputs. Every application accepts one input: configuration. Possible additional inputs would be TLS certficates, files to be converted, etc.

### Configuration

Configuration is anything that is likely to vary between deployments. This could be:

- connections to databases, key-value-stores or other external dependencies
- credentials and secrets
- scoped configuration like hostnames, tenant information, etc

Static configuration in the code is an anti-pattern.

Configuration for an application is stored in **environment variables** (env vars). Environment variables are easy to change between deployments without changing any code. Additionally, they are a language- and OS-agnostic standard that "works everywhere".

> Configuration must be checked for validity on application startup as part of an internal healthcheck.

![Configuration](./img/codebase-deploys.png)

## Outputs

An application will most likely produce an output of some kind: messages to STDOUT, changes to a database or file, creation of artifacts. These should be described informally to enable functional verification of the application.

## Dependencies

An application can have multiple dependencies. These could be libraries and packages but also databases and key-value stores.

An application never relies on implicit existence of any dependency or package. Instead, dependencies must be explicitly declared.

An application makes no difference between local or remote environments and treats every external dependency as an attached resource that can be accessed with a URL or other means.

> External dependencies must be checked on application startup as part of an internal healthcheck.

Deploying to different environments is as easy as configuring different connection information and credentials for the external dependencies though environment variables.

![Attached Resources](./img/attached-resources.png)