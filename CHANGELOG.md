### [1.8.1](https://gitlab.com/ayedocloudsolutions/adf/compare/v1.8.0...v1.8.1) (2021-09-14)


### :bug: Fixes

* stuff ([62ab6d8](https://gitlab.com/ayedocloudsolutions/adf/commit/62ab6d829247f315ef36531da41c40c7ad112819))

## [1.8.0](https://gitlab.com/ayedocloudsolutions/adf/compare/v1.7.2...v1.8.0) (2021-09-14)


### :repeat: Chores

* added docs for metrics, healthchecks and traces ([59d7bb6](https://gitlab.com/ayedocloudsolutions/adf/commit/59d7bb66f2a4a5abfa12db1332ee9624f5e64700))


### :repeat: CI

* corrected pattern matching for job conditions ([d311009](https://gitlab.com/ayedocloudsolutions/adf/commit/d311009ca6857377b431d0926a50ec2128ab3db4))
* make registry credentials configurable ([09eff9d](https://gitlab.com/ayedocloudsolutions/adf/commit/09eff9d7dff65f8e020b7a214d4cae8d3f824d4e))
* new stuff ([5180ca4](https://gitlab.com/ayedocloudsolutions/adf/commit/5180ca4435853324486fdc21254c5182a6653314))
* new stuff ([e89e52c](https://gitlab.com/ayedocloudsolutions/adf/commit/e89e52c901306c94c4208cc071c9bd730f03f64b))
* new stuff ([8810578](https://gitlab.com/ayedocloudsolutions/adf/commit/8810578ae4b46fcaeed23411e2b0d032b520a9ba))
* new stuff ([d084cc2](https://gitlab.com/ayedocloudsolutions/adf/commit/d084cc27f91fc37add1a75693d63b5b7d2f0a82a))
* new stuff ([923ffd0](https://gitlab.com/ayedocloudsolutions/adf/commit/923ffd05d31089b66b1accf952782b0683b44402))
* new stuff ([906c89c](https://gitlab.com/ayedocloudsolutions/adf/commit/906c89c7acee053e75cf14da19eb9d4f0e0acced))
* new stuff ([e6b94d7](https://gitlab.com/ayedocloudsolutions/adf/commit/e6b94d78792195a95bea7967916535898d559b00))
* new stuff ([de72752](https://gitlab.com/ayedocloudsolutions/adf/commit/de72752796c5c24c0d03d827d7d219a1ef848fd6))
* new stuff ([67e3ee3](https://gitlab.com/ayedocloudsolutions/adf/commit/67e3ee3623316a9ca688b58c88ab3ab63a851328))
* new stuff ([7284e8a](https://gitlab.com/ayedocloudsolutions/adf/commit/7284e8a720ba67c18f754879161c630d1536684a))
* new stuff ([cfbf167](https://gitlab.com/ayedocloudsolutions/adf/commit/cfbf167578202da49f0977536c729976934b571a))
* new stuff ([3ba51dd](https://gitlab.com/ayedocloudsolutions/adf/commit/3ba51dd376c737542cd5b4efcbd725a69b8c5d8a))
* new stuff ([da9c42d](https://gitlab.com/ayedocloudsolutions/adf/commit/da9c42d11957d8e65a4b018cf83dced82bf9aeb8))
* new stuff ([0497d17](https://gitlab.com/ayedocloudsolutions/adf/commit/0497d174a6cd70459f054a1ddefe1308f9d07e24))
* new stuff ([95f9d3a](https://gitlab.com/ayedocloudsolutions/adf/commit/95f9d3a4fedeaaa2f209fea83af53f2166ce1f54))
* new stuff ([14886e4](https://gitlab.com/ayedocloudsolutions/adf/commit/14886e4d1b5a9422e0ae4cda146ad2b4e930a289))
* new stuff ([fdafe31](https://gitlab.com/ayedocloudsolutions/adf/commit/fdafe316cb61045248cf5b11dd546d980e9fd8e6))
* new stuff ([1ac878a](https://gitlab.com/ayedocloudsolutions/adf/commit/1ac878a26a6d1b75ef182ed91debd2306b880ec5))
* new stuff ([d8ccd17](https://gitlab.com/ayedocloudsolutions/adf/commit/d8ccd173fde04cc677f3b8577e3f48031fdad882))
* new stuff ([b2369d1](https://gitlab.com/ayedocloudsolutions/adf/commit/b2369d15db55e524279202658d7d7494fd5dba6f))
* new stuff ([b7b5fc5](https://gitlab.com/ayedocloudsolutions/adf/commit/b7b5fc5fb76e559d8288422c7f7493db62e98257))
* new stuff ([af7ef71](https://gitlab.com/ayedocloudsolutions/adf/commit/af7ef71c9475cf4d332b230cec6396486c7e7551))
* new stuff ([b9202bd](https://gitlab.com/ayedocloudsolutions/adf/commit/b9202bd33bd85a3f255b03a0643ce8bd4996d716))
* update afd version ([0bd34ed](https://gitlab.com/ayedocloudsolutions/adf/commit/0bd34ed79f3b3c05c132660248e9cd90550402a8))


### :zap: Refactoring

* move code to shipmate.sh ([433182a](https://gitlab.com/ayedocloudsolutions/adf/commit/433182aaadf27cae0e91e74c86d289fd5f8e8b83))
* move code to shipmate.sh ([44a2ecc](https://gitlab.com/ayedocloudsolutions/adf/commit/44a2eccfc50d9a1c41febc5526d1f5c3f84950ee))
* removed clutter ([193aecd](https://gitlab.com/ayedocloudsolutions/adf/commit/193aecd81b213c26af67f77c27c04d9b02333a0f))


### :memo: Documentation

* add override ([cc520f4](https://gitlab.com/ayedocloudsolutions/adf/commit/cc520f493ba2f44e2cdcdce7cf7df56f28471eff))


### :bug: Fixes

* ci ([d445c62](https://gitlab.com/ayedocloudsolutions/adf/commit/d445c62f5311f0924767b54685501b491dae4c94))
* ci ([2d20e7b](https://gitlab.com/ayedocloudsolutions/adf/commit/2d20e7b300b70d3112bf0ace2c25ea58e0ec928e))
* corrected markdown syntax in script ([f6eff61](https://gitlab.com/ayedocloudsolutions/adf/commit/f6eff61837051f30310feaa3f3f00d32ee9aeabf))
* corrected markdown syntax in script ([3aced68](https://gitlab.com/ayedocloudsolutions/adf/commit/3aced687997d9b1202bb7d5b8579d332dd8a37c0))
* fixed incorrect build.package stage ([d5f8a03](https://gitlab.com/ayedocloudsolutions/adf/commit/d5f8a03f6d3921497c27e71eed76b88800d114a6))
* IMAGE_NAME falsely overwritten in build.docker ([17ee70a](https://gitlab.com/ayedocloudsolutions/adf/commit/17ee70a6e11c172b893e7ebedcdc83cc39e9d237))
* pipeline ([6ab893d](https://gitlab.com/ayedocloudsolutions/adf/commit/6ab893d2f5c37eed93c0571a29a13d678460a6c6))
* refactore stuff ([ab56281](https://gitlab.com/ayedocloudsolutions/adf/commit/ab56281579162639733c2d07967099ad30d5bc82))
* refactored functions ([8229192](https://gitlab.com/ayedocloudsolutions/adf/commit/8229192f531e202725ce3c6204592ac943235501))
* refactored functions ([571148c](https://gitlab.com/ayedocloudsolutions/adf/commit/571148cc32220c860cc9f02de54781d2c36719b9))
* removed legacy functions ([c404262](https://gitlab.com/ayedocloudsolutions/adf/commit/c40426220f6a37abb8c2a7e3197f7669102e110d))
* replace sourcing mechanism ([2ccecae](https://gitlab.com/ayedocloudsolutions/adf/commit/2ccecae9ea9420c3c50abc2b74a3ddc62c8453fb))
* stuff ([ae9a61d](https://gitlab.com/ayedocloudsolutions/adf/commit/ae9a61dd22ccb9cfb00b2a2ae911a039e12b770b))
* stuff ([fe4b55b](https://gitlab.com/ayedocloudsolutions/adf/commit/fe4b55bdc66baa02794518c028bd1b92c216121a))
* stuff ([84429ea](https://gitlab.com/ayedocloudsolutions/adf/commit/84429eaff1dbc50081cbecce670103d81be9db73))
* stuff ([5a89b6e](https://gitlab.com/ayedocloudsolutions/adf/commit/5a89b6e759b8d2b5480577fcf1f58f89d0df1b2b))
* stuff ([cbe87fe](https://gitlab.com/ayedocloudsolutions/adf/commit/cbe87fe626aed6e4a0ce29d8ea6782fdc47f1c03))
* stuff ([1f01744](https://gitlab.com/ayedocloudsolutions/adf/commit/1f017447ad2d9da33ac96f9a12531755e5882810))
* stuff ([9c28d77](https://gitlab.com/ayedocloudsolutions/adf/commit/9c28d7734499449ffdb877675a6faaab7796833b))
* stuff ([ebafa22](https://gitlab.com/ayedocloudsolutions/adf/commit/ebafa225e9c361c382fc19543432939ddda862f3))
* stuff ([68776dd](https://gitlab.com/ayedocloudsolutions/adf/commit/68776ddff469488c99f49422991e54aa02bb85f5))
* updated markdown ([d53e9aa](https://gitlab.com/ayedocloudsolutions/adf/commit/d53e9aa0a6526f84ac8531fe5aded7fa000686b5))
* updated markdown ([b2c9bf9](https://gitlab.com/ayedocloudsolutions/adf/commit/b2c9bf9f752a69b2f8d4dd6f208cf1ac5d948509))


### :sparkles: Features

* added Apprise to Docker image ([f74dca5](https://gitlab.com/ayedocloudsolutions/adf/commit/f74dca54c7239137c996c01302e7aa9dd54f91e4))
* added logo ([6f4049a](https://gitlab.com/ayedocloudsolutions/adf/commit/6f4049ada8999920db0b180e5552b96d78629a00))
* added logo ([6b25627](https://gitlab.com/ayedocloudsolutions/adf/commit/6b25627f374c27e4688075073436a3df8bab49ed))

### [1.7.2](https://gitlab.com/ayedocloudsolutions/adf/compare/v1.7.1...v1.7.2) (2021-08-25)


### :bug: Fixes

* added stable to release stage ([0b95443](https://gitlab.com/ayedocloudsolutions/adf/commit/0b9544304e22827de0e7e0a47ef6f674d9f259d9))
* refactored comment-mr ([73bf1e4](https://gitlab.com/ayedocloudsolutions/adf/commit/73bf1e4df28431516fc680ba7757454dff34949a))
* refactored comment-mr ([3d7c155](https://gitlab.com/ayedocloudsolutions/adf/commit/3d7c155f4b9b19a197bd34910fe85a66bf17760a))
* refactored comment-mr ([f1d366f](https://gitlab.com/ayedocloudsolutions/adf/commit/f1d366fd4fc8f6dc0994489f424418f95fc26d54))
* wrong bash ([660edb9](https://gitlab.com/ayedocloudsolutions/adf/commit/660edb98c489a1dd0087d7993932117e4278d021))
* wrong bash ([b81ab1b](https://gitlab.com/ayedocloudsolutions/adf/commit/b81ab1b4815ad21baacd099e7373f131f3dc7063))
* wrong bash ([13fdebd](https://gitlab.com/ayedocloudsolutions/adf/commit/13fdebd9ba5bfa0c707efd5e441bce48d9a00f99))

### [1.7.1](https://gitlab.com/ayedocloudsolutions/adf/compare/v1.7.0...v1.7.1) (2021-08-25)


### :repeat: CI

* changed release config ([4c41e2c](https://gitlab.com/ayedocloudsolutions/adf/commit/4c41e2ceac283841756046290e45d6f9c556ba0b))
* changed release config ([19b5c3f](https://gitlab.com/ayedocloudsolutions/adf/commit/19b5c3f1035767bdf37b417d9fd16634977a0711))
* changed release config ([de363bf](https://gitlab.com/ayedocloudsolutions/adf/commit/de363bf9b4de6e8e7686682fc3045ad145184140))
* moved pre-release logic to release stage ([b134c39](https://gitlab.com/ayedocloudsolutions/adf/commit/b134c3946b4e94ba6244b69248aa45e7690eb8aa))
* remove clutter ([d91e037](https://gitlab.com/ayedocloudsolutions/adf/commit/d91e03761261baf1a68f5efb43db91c67082c474))
* remove clutter ([3d38c2d](https://gitlab.com/ayedocloudsolutions/adf/commit/3d38c2d717dc093a5646425abbac0785c5bfcdc1))
* remove clutter ([b3f92aa](https://gitlab.com/ayedocloudsolutions/adf/commit/b3f92aa03980761974ec9e8d8f6ad40960e3e132))
* remove clutter ([20d8a62](https://gitlab.com/ayedocloudsolutions/adf/commit/20d8a620306b57ede8825f1a27f52439f9ff6b46))
* remove clutter ([51a3be2](https://gitlab.com/ayedocloudsolutions/adf/commit/51a3be2bcc19163dd97e86e5e9eae834fbf8c4ba))
* remove clutter ([b8e6b8b](https://gitlab.com/ayedocloudsolutions/adf/commit/b8e6b8bd3066ea26f59cbcc492c935376aedbaf7))
* remove clutter ([ed5f761](https://gitlab.com/ayedocloudsolutions/adf/commit/ed5f761be31cae4e6d23b4895754e3169e952fc5))
* remove clutter ([a686412](https://gitlab.com/ayedocloudsolutions/adf/commit/a68641218717690249837ff8300f0f9dc7c817ee))
* remove clutter ([c354a86](https://gitlab.com/ayedocloudsolutions/adf/commit/c354a8653dde1a5c4d60850892cb0459309efcf1))
* remove clutter ([cfb2e60](https://gitlab.com/ayedocloudsolutions/adf/commit/cfb2e602b7cc9bf5dbff0f7290b5975601bea829))
* remove clutter ([6c27223](https://gitlab.com/ayedocloudsolutions/adf/commit/6c272233251bcd79c36fb31fdd7cd0065effb717))
* remove clutter ([9444d8f](https://gitlab.com/ayedocloudsolutions/adf/commit/9444d8fa76160d8064dcd930c180740e1a006fac))
* remove clutter ([66318d4](https://gitlab.com/ayedocloudsolutions/adf/commit/66318d477ac0cb797438cd815515d1f53d757e99))
* remove clutter ([4298074](https://gitlab.com/ayedocloudsolutions/adf/commit/42980744555fe84db99609da793659135b9495d3))
* remove clutter ([fcccf53](https://gitlab.com/ayedocloudsolutions/adf/commit/fcccf53d2ec0f232bfe4b260391d77c6a71ed6fc))
* removed old config ([3b962d6](https://gitlab.com/ayedocloudsolutions/adf/commit/3b962d6386b1b989a8c13ca267d90ba7c84011bc))
* stuff ([450f913](https://gitlab.com/ayedocloudsolutions/adf/commit/450f913b829fcdbdc0656a500017cfbf4fddd3c3))
* stuff ([118fff7](https://gitlab.com/ayedocloudsolutions/adf/commit/118fff75a35df7f09c454037ac9715eea82c67e9))
* stuff ([0b58d64](https://gitlab.com/ayedocloudsolutions/adf/commit/0b58d642610160e0ed7265e3e6d4b867ba5c4a62))
* stuff ([21d037c](https://gitlab.com/ayedocloudsolutions/adf/commit/21d037c7b4ba91a9efd8b1241e0ab884d46bcb4d))
* stuff ([d2f2102](https://gitlab.com/ayedocloudsolutions/adf/commit/d2f21025718ca439b519415ca0177b08e86399e0))
* stuff ([30fc1d8](https://gitlab.com/ayedocloudsolutions/adf/commit/30fc1d8853ef15619cb143e1cec21182190cecde))
* stuff ([315d011](https://gitlab.com/ayedocloudsolutions/adf/commit/315d0114ba48e30af8031f55e593597eb7d63896))
* stuff ([285af39](https://gitlab.com/ayedocloudsolutions/adf/commit/285af398ea19ec14ace674a4f373c31b254d2ddc))
* stuff ([3a6e513](https://gitlab.com/ayedocloudsolutions/adf/commit/3a6e513d9e0f5dff7e40b662c5047537e898abed))
* switched to MR pipelines ([4c65fa2](https://gitlab.com/ayedocloudsolutions/adf/commit/4c65fa2d058d1974e5ea5f2999e3dfddd0484441))


### :bug: Fixes

* added script logic ([ebb3a02](https://gitlab.com/ayedocloudsolutions/adf/commit/ebb3a02a7855bc998ab2120b9828bffaae36c8ce))
* added script logic ([311c880](https://gitlab.com/ayedocloudsolutions/adf/commit/311c8807cb8806c4837b5ce573aeff96155d189a))
* added script logic ([17905d7](https://gitlab.com/ayedocloudsolutions/adf/commit/17905d7eda4206a65ed1ccd8b1de21b118e6b185))
* added script logic ([0920dee](https://gitlab.com/ayedocloudsolutions/adf/commit/0920deef3f38d937a9a1bea88d7582b7e9ed3854))
* changed release config ([93c99ab](https://gitlab.com/ayedocloudsolutions/adf/commit/93c99abfb3f45cca30ec420438b526fae6cd10f3))
* changed release config ([f729830](https://gitlab.com/ayedocloudsolutions/adf/commit/f729830ab3c622be403684518a5685a5145d3f1a))
* stuff ([e9c7ef5](https://gitlab.com/ayedocloudsolutions/adf/commit/e9c7ef5ea4028ece4d1eec33653a08e8bb508fc2))
* stuff ([7ccbd67](https://gitlab.com/ayedocloudsolutions/adf/commit/7ccbd67f0020e3c833305b148919a96373ccaed7))

* internal/config: migrate local config to `.git/glab-cli`  #813

* goreleaser: fix nfpms install dir to use /usr/bin  #817
