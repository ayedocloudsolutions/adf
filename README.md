# A DevOps Framework

## Add npm module

```bash
npm install
npm ci
```

## Build Docker Multiplatform (experimental)

```bash
docker buildx build --platform linux/amd64 -t adf .
```

## Setup local tooling

```bash
brew install hadolint
```

## Earthly SSH Setup

```bash
eval `ssh-agent`
ssh-add
```
